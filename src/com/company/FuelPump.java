package com.company;

public class FuelPump {

    /**
     * <Strong>This is used for getting the information about the amount of the counter of this FuelPump</Strong>
     * @return This method returns the amount of fuel that has passed through this fuelPump
     */
    public double getCounter() {
        //I return the counters value
        return counterL;
    }

    /**
     * <Strong>This is used for adding a amount to this FuelPump counter</Strong>
     * @param amountL The amount of fuel in L the counter need to add
     * @return This method returns true if you can add the amount and false if you can't add the amount
     */
    public boolean addToCounter(double amountL) {
        //Here I try to remove some fuel from the connected tank, if this fails then this method will fail and take you back
        if (connectedTank.removeFromAmountL(amountL)){
            counterL += amountL;
            return true;
        } else {
            return false;
        }
    }

    /**
     * <Strong>This is used for getting the information about the amount that this FuelPump has earned</Strong>
     * @param pricePrL The price that gets used to calculate the total earnings of this FuelPump
     * @return This method returns the amount of money this FuelPump has made
     */
    public int getTurnover(int pricePrL){

        //Here I calculate the earnings of the FuelPump and then return it
        int earnings = (int)counterL*pricePrL;
        return earnings;
    }

    //The double contains the counters current value and the Tank connectedTank contains this FuelPumps connected Tank object
    private double counterL;
    private Tank connectedTank;

    /**
     * <Strong>This is the constructor for the FuelPump class</Strong>
     * @param connectedTank The Tank that this FuelPump is connected to
     */
    public FuelPump(Tank connectedTank){
        this.connectedTank = connectedTank;
    }


}
