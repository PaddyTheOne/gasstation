package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class ScanOptions {

    //Colors for the console text
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_WHITE = "\u001B[37m";
    // Colors for the console background
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";
    public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
    public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
    // Some pre-made coloring options
    public static final String ANSI_fill = ANSI_BLACK_BACKGROUND+ANSI_BLACK;
    public static final String ANSI_wrong = ANSI_RED_BACKGROUND+ANSI_BLACK;
    public static final String ANSI_userText = ANSI_WHITE_BACKGROUND + ANSI_BLACK;
    public static final String ANSI_finalResult = ANSI_BLACK_BACKGROUND + ANSI_YELLOW;
    public static final String ANSI_correct = ANSI_GREEN_BACKGROUND + ANSI_BLACK;
    public static final String ANSI_userTextCurrent = ANSI_BLUE_BACKGROUND + ANSI_BLACK;
    public static final String ANSI_computerText = ANSI_BLACK_BACKGROUND+ANSI_GREEN;


    /**
     * <Strong>This is used for inputs from the user, it prints a list of options and then prompts the user for a choice</Strong>
     * @return This method returns the choice in int, via the selected strings position
     * @param headLine The text you want to print before the userpromt
     * @param options The Arraylist with the different options
     */
    public static int selectBox(String headLine, ArrayList<String> options){

        int  Num=0, selected=0;
        String choice="";
        boolean found = false;
        String selectedString=options.get(selected);

        //Here I'm defining a new scanner
        Scanner keyboard = new Scanner(System.in);
        //Here I have made a while-loop, because the user must use a value that is within the list
        while (!found){
            System.out.println(ANSI_computerText+"\n\n\n\n"+headLine);
            /* Here I'm using a for-loop to print all the different options in the list. I have added a "TAB"-function so the selected line need to show what line is selected via the TAB function
            The "TAB" function works if you use a (TAB+ENTER) and then just (ENTER), When the blue line is on the line you want to select */
            for (String i : options){
                if (options.get(Num).equals(selectedString)){
                    if (Num<9){
                        System.out.println(ANSI_userTextCurrent+"0"+(Num+1)+" - "+i);
                    } else {
                        System.out.println(ANSI_userTextCurrent+(Num+1)+" - "+i);
                    }
                } else {
                    if (Num<9){
                        System.out.println(ANSI_userText + "0" + (Num + 1) + " - " + i);
                    } else {
                        System.out.println(ANSI_userText + (Num + 1) + " - " + i);
                    }
                }
                Num++;
            }
            System.out.println(ANSI_fill);
            choice = keyboard.nextLine();

            /* Here the users input gets evaluated -
            If the LINE contains a TAB, then the blue line will be moved, when the loop restarts */
            if (choice.equals("\t")){
                if (options.size()-1==selected){
                    selected=0;
                } else {
                    selected+=1;
                }
                selectedString=options.get(selected);
                Num=0;
            } else {//If the LINE isn't a TAB, then it will check for a ENTER
                if (choice.equals("")) {
                    found = true;
                    selected += 1;
                    choice = Integer.toString(selected);
                } else if (TRYFORNUMBER(choice)) {//If the LINE doesn't contain anything or TAB, then it will be tested if it is a number
                    if (options.size() >= Integer.parseInt(choice)) {
                        found = true;
                    } else {//Gets printed if the choice isn't a number
                        System.out.println(ANSI_wrong + "\n\nError - Unable To Find\n");
                        Num = 0;
                        selected = 0;
                    }
                } else {//Gets printed if the choice isn't within the available values
                    System.out.println(ANSI_wrong + "\n\nError - Not A Value\n");
                    Num = 0;
                    selected = 0;
                }
            }
        }
        //If everything is "A OK" then the choice will be returned
        return Integer.parseInt(choice);
    }

    /**
     * <Strong>This is used for inputs from the user, it prints a text and then prompts the user for an answer</Strong>
     * @return This method returns the string, which the user inputted via the scanner
     * @param headline The text you want to print before the userpromt
     * @param minLength The minimum length the string can be, otherwise it will not be accepted
     * @param maxLength The maximum lenght the string can be, otherwise it will not be accepted
     */
    public static String selectString(String headline, int minLength, int maxLength){

        Scanner choice = new Scanner(System.in);
        String result="";
        boolean runAtLeastOnceBefore=false;

        //This is a failsafe, so the input to the sql server doesn't get to long for the containers. (It is a checker & repeater with 2 error messages)
        do {
            if (result.length()>maxLength && runAtLeastOnceBefore){
                System.out.println(ANSI_wrong+"ERROR: String can't be longer then "+maxLength+" chars");
            }
            if (result.length()<minLength && runAtLeastOnceBefore){
                System.out.println(ANSI_wrong+"ERROR: String can't be shorter then "+minLength+" chars");
            }
            System.out.print(ANSI_userText+headline+": ");
            System.out.print(ANSI_RESET+" ");
            result=choice.nextLine();
            System.out.println(ANSI_fill+"");
            runAtLeastOnceBefore=true;
        } while (result.length()>maxLength || result.length()<minLength);

        return result;
    }

    /**
     * <Strong>This is used for inputs from the user, it prints a text and then prompts the user for an answer</Strong>
     * @return This method returns the int, which the user inputted via the scanner
     * @param headline The text you want to print before the userprompt
     * @param minValue The minimum value the int can be, otherwise it will not be accepted
     * @param maxValue The maximum value the int can be, otherwise it will not be accepted
     */
    public static int selectInt(String headline, int minValue, int maxValue){

        Scanner choice = new Scanner(System.in);
        String result="0";
        boolean runAtLeastOnceBefore=false;

        //This is a failsafe, so the input isn't over the specified amount and also if it is a number (It is 2 checkers & repeaters with 3 error messages)
        do {
            if (Integer.parseInt(result)>maxValue && runAtLeastOnceBefore){
                System.out.println(ANSI_wrong+"ERROR: Value can't be more then "+maxValue);
            }
            if (Integer.parseInt(result)<=minValue && runAtLeastOnceBefore){
                System.out.println(ANSI_wrong+"ERROR: Value can't be less then "+minValue);
            }
            System.out.print(ANSI_userText+headline + ":");
            System.out.print(ANSI_RESET+" ");
            do {
                if (!TRYFORNUMBER(result)){
                    System.out.println(ANSI_wrong+"ERROR: Not a value");
                    System.out.print(ANSI_userText+headline + ":");
                    System.out.print(ANSI_RESET+" ");
                }
                result = choice.nextLine();
                System.out.println(ANSI_fill+"");
                runAtLeastOnceBefore=true;
            } while (!TRYFORNUMBER(result));
        } while (Integer.parseInt(result)>maxValue || Integer.parseInt(result)<minValue);

        return Integer.parseInt(result);
    }

    /**
     * <Strong>This is used for inputs from the user, it prints a text and then prompts the user for an answer</Strong>
     * @return This method returns the int, which the user inputted via the scanner
     * @param headline The text you want to print before the userprompt
     */
    public static boolean selectBoolean(String headline){

        Scanner choice = new Scanner(System.in);
        String result="0";
        boolean runAtLeastOnceBefore=false;

        //This is a failsafe, so the input isn't over the specified amount and also if it is a number (It is 2 checkers & repeaters with 3 error messages)
        do {
            if (Integer.parseInt(result)>1 && runAtLeastOnceBefore){
                System.out.println(ANSI_wrong+"ERROR: Value can't be more then "+1);
            }
            if (Integer.parseInt(result)<=0 && runAtLeastOnceBefore){
                System.out.println(ANSI_wrong+"ERROR: Value can't be less then "+0);
            }
            System.out.print(ANSI_userText+headline + ":");
            System.out.print(ANSI_RESET+" ");
            do {
                if (!TRYFORNUMBER(result)){
                    System.out.println(ANSI_wrong+"ERROR: Not a value");
                    System.out.print(ANSI_userText+headline + ":");
                    System.out.print(ANSI_RESET+" ");
                }
                result = choice.nextLine();
                System.out.println(ANSI_fill+"");
                runAtLeastOnceBefore=true;
            } while (!TRYFORNUMBER(result));
        } while (Integer.parseInt(result)>1 || Integer.parseInt(result)<0);

        if (Integer.parseInt(result)==1){
            return true;
        } else {
            return false;
        }
    }

    /**
     * <Strong>This is used for showing errors</Strong>
     * @param errormessage The string that is shown as the error
     */
    public static void showError(String errormessage){
        //The errormessage gets printed between 2 lines
        System.out.println(ANSI_wrong+"\n");
        System.out.println(ANSI_wrong+errormessage);
        System.out.println(ANSI_wrong+"\n");
    }

    /**
     * <Strong>This is used for showing errors</Strong>
     * @param headline The string that is shown as the error
     * @param borderAmount The amount of border space that is put after the string
     */
    public static void showString(String headline,int borderAmount){

        //The headline gets printed
        System.out.println(ANSI_computerText+headline);

        //The black border thats under the headline gets printed
        if (borderAmount>0){
            for (int i = 0 ; borderAmount>i; i++){
                System.out.println(ANSI_fill);
            }
        }
    }

    /**
     * <Strong>This is used for checking if the string can be parsed as an int</Strong>
     * @return This method returns the string, if it can be parsed as an int
     * @param possibleValue The value that needs to be checked
     */
    public static boolean TRYFORNUMBER (String possibleValue){
        try {
            Integer.parseInt(possibleValue);
            return true;
        } catch (NumberFormatException ex){
            return false;
        }
    }

}


