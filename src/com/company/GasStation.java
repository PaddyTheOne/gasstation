package com.company;

import java.util.ArrayList;

public class GasStation {

    /***
     * @author Patrick G. Schemel
     * @version 1.0.0
     */


    /**
     * <Strong>This is the main method</Strong>
     */
    public static void main(String[] args) {

        //This is my list for the not known amount of fuelPumps and another for tanks
        ArrayList<FuelPump> stationPumps = new ArrayList<>();
        Tank mainTank = new Tank(1000, 1000);

        //Here the Gas-station gets setup
        setup(stationPumps,mainTank);

        //Here the program begins
        program(stationPumps,mainTank);

    }

    /**
     * <Strong>This is used for setting up the amount of pumps and tanks</Strong>
     * @param fuelPumpArrayList The list with the different fuel pumps
     * @param tank The tank that the Gasstation uses as mainTank
     */
    public static void setup(ArrayList<FuelPump> fuelPumpArrayList, Tank tank){

        //Here you decide how many tanks you want on the station
        int amount = ScanOptions.selectInt("Select amount of FuelPumps",1,100000);

        //This adds the amount of fuelPumps you want
        for (int i = 0; amount > i; i++){
            //Here I'm adding another new fuelPump to the station
            fuelPumpArrayList.add(new FuelPump(tank));
        }

    }

    /**
     * <Strong>This is the program, that does the socalled "simulation" as you can do things with the objects</Strong>
     * @param fuelPumpArrayList The list with the different fuel pumps
     * @param tank The tank that the Gasstation uses as mainTank
     */
    public static void program(ArrayList<FuelPump> fuelPumpArrayList,Tank tank){

        //This is my list with options in the main menu
        ArrayList<String> mainMenu = new ArrayList<>();

        //This boolean tells the program to end if it is true
        boolean endProgram = false;

        //This is the price given to the fuel (DKK pr Liter), but it is only here to be initialized
        int price=0;

        //Here I'm adding the different options in the main menu
        mainMenu.add("Use FuelPump");
        mainMenu.add("Fill Up Tank");
        mainMenu.add("End Program");

        //Here is the option part of the program, where you can take different actions
        do{
            //You get to chose
            int choice = ScanOptions.selectBox("Select Process",mainMenu);

            //Based on the choice something happens
            switch (choice){
                case 1:
                    //This starts the useFuelPump method with the list of FuelPumps
                    useFuelPump(fuelPumpArrayList);
                    break;
                case 2:
                    //This starts the fillTank method with the mainTank
                    fillTank(tank);
                    break;
                case 3:
                    //This gives you the choice of price for the fuel and then ends this do-while-loop
                    price = ScanOptions.selectInt("Price for fuel (DKK pr Liter)",1,10000);
                    endProgram=true;
                    break;
            }
        } while (!endProgram);

        //HERE I START PRINTING INFORMATION

        //This is where the Tank's information gets printed with one blank line underneath
        ScanOptions.showString("Tank:",0);
        ScanOptions.showString("mainTank - "+tank.getAmountL()+"L/1000L",1);

        //This is where the FuelPumps' information gets printed
        ScanOptions.showString("Pumps:",0);
        for (int i = 0; i<fuelPumpArrayList.size();i++){
            ScanOptions.showString("Pump"+(i+1)+" - "+fuelPumpArrayList.get(i).getCounter()+"L  /  Earnings - "+fuelPumpArrayList.get(i).getTurnover(price),0);
        }
    }

    /**
     * <Strong>This is used for the action using the different fuelPumps</Strong>
     * @param fuelPumpArrayList The list with the different fuel pumps
     */
    public static void useFuelPump(ArrayList<FuelPump> fuelPumpArrayList){

        //This is the list of pumps that can be chosen from
        ArrayList<String> listOfPumps = new ArrayList<>();

        //Here the pumps gets added to the list
        for (int i = 0; fuelPumpArrayList.size() > i; i++){
            listOfPumps.add("Pump"+(i+1));
        }

        //Here you chose which pump you want to use
        int choice = ScanOptions.selectBox("Select Pump",listOfPumps);

        //Here you try to fuel your car if it doesn't work then you get returned to the main menu
        fuelPumpArrayList.get(choice-1).addToCounter(ScanOptions.selectInt("Amount of fuel L",1,100000));
    }

    /**
     * <Strong>This is used for the action of filling up a x amount of fuel into the mainTank</Strong>
     * @param tank The tank that the Gasstation uses as mainTank
     */
    public static void fillTank(Tank tank){

        int amount;

        do{
            amount = ScanOptions.selectInt("Fill amount in tank (L)",1,100000);
        } while(!tank.addToAmountL(amount));

    }




}
