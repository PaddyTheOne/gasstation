package com.company;

public class Tank {

    /**
     * <Strong>This is used for getting the information about the amount of fuel in this tank</Strong>
     * @return This method returns the amount of fuel that is in this tank
     */
    public double getAmountL() {
        //I return the tanks amountL
        return tankAmountL;
    }

    /**
     * <Strong>This is used for adding a amount to this Tank</Strong>
     * @param amountL The amount of fuel in L you want to add
     * @return This method returns true if you can add the amount and false if you can't add the amount
     */
    public boolean addToAmountL(double amountL) {
        //Here I check if it is possible to add that amount otherwise it will fail and return false
        if ((this.tankAmountL+amountL)>tankMaxAmountL){
            ScanOptions.showError("Can't be filled with ("+amountL+"L) as the current amount of this Tank is ("+this.tankAmountL+"L/"+tankMaxAmountL+"L) - you can max add: "+(tankMaxAmountL-this.tankAmountL)+"L");
            return false;
        } else {
            this.tankAmountL += amountL;
            return true;
        }
    }

    /**
     * <Strong>This is used for removing a amount from this Tank</Strong>
     * @param amountL The amount of fuel in L you want to remove
     * @return This method returns true if you can remove the amount and false if you can't remove the amount
     */
    public boolean removeFromAmountL(double amountL) {
        //Here I check if it is possible to remove that amount otherwise it will fail and return false
        if ((this.tankAmountL-amountL)<0){
            ScanOptions.showError("Can't take this amount ("+amountL+"L) as the current amount of this Tank is ("+this.tankAmountL+"L/"+tankMaxAmountL+"L) - you can max remove: "+(this.tankAmountL)+"L");
            return false;
        } else {
            this.tankAmountL -= amountL;
            return true;
        }
    }

    //These are the doubles that contains the tanks amount and max amount
    private double tankMaxAmountL;
    private double tankAmountL;

    /**
     * <Strong>This is the constructor for the Tank class</Strong>
     * @param amountL The amount of fuel in L the Tank has
     * @param amountMaxL The amount of fuel in L the Tank maximum can have at one time
     */
    public Tank(double amountL, double amountMaxL){
        this.tankAmountL = amountL;
        this.tankMaxAmountL = amountMaxL;
    }

}
